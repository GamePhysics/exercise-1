#include "Spring.h"
#include <math.h>


Spring::Spring(void)
{
}

Spring::Spring(Point *p1, Point *p2)
{
	stiffness = 50;
	point1 = p1;
	point2 = p2;
	initialLength = currentLength();
}


Spring::~Spring(void)
{
}

float Spring::currentLength(){
	float a = point1->position->x - point2->position->x;
	float b = point1->position->y - point2->position->y;
	float c = point1->position->z - point2->position->z;
	return sqrt(a*a + b*b + c*c);
}

void Spring::computeElasticForces(){
	DirectX::XMFLOAT3 *force = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	float currentLen = currentLength();
	float newton = -stiffness * (currentLen - initialLength);
	// normalisieren des Vectors und ausrechnen der Kraft


	force->x = newton * (point1->position->x - point2->position->x) / currentLen;
	force->y = newton * (point1->position->y - point2->position->y) / currentLen;
	force->z = newton * (point1->position->z - point2->position->z) / currentLen;
	//bis hier hin stimmts

	addToEndPoints(force);

	// D�mpfung:


	//Betrag der Geschwindigkeit ausrechnen und normalisieren des Geschwindigkeitsvectors
	float a1 = point1->velocity->x;
	float b1 = point1->velocity->y;
	float c1 = point1->velocity->z;
	float v1 =  sqrt(a1*a1 + b1*b1 + c1*c1);

	a1 = a1/v1;
	b1 = b1/v1;
	c1 = c1/v1;


	float a2 = point2->velocity->x;
	float b2 = point2->velocity->y;
	float c2 = point2->velocity->z;
	float v2 =  sqrt(a2*a2 + b2*b2 + c2*c2);

	a2 = a2/v2;
	b2 = b2/v2;
	c2 = c2/v2;


	//ALT:   
	point1->force->x -= point1->damping * point1->velocity->x;
	point1->force->y -= point1->damping * point1->velocity->y;
	point1->force->z -= point1->damping * point1->velocity->z;

	point2->force->x -= point2->damping * point2->velocity->x;
	point2->force->y -= point2->damping * point2->velocity->y;
	point2->force->z -= point2->damping * point2->velocity->z;
	

	/*
	//Neu: verbuggt iwie das programm, sollte aber eig physikalisch richtig sein
	point1->force->x -= point1->damping * a1;
	point1->force->y -= point1->damping * b1;
	point1->force->z -= point1->damping * c1;

	point2->force->x -= point2->damping * a2;
	point2->force->y -= point2->damping * b2;
	point2->force->z -= point2->damping * c2;
	
	*/


	free(force);
}

void Spring::computeElasticForcesMidPoint(){
	// normalisieren des Vectors und ausrechnen der Kraft
	DirectX::XMFLOAT3 *force = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	
	float a = point1->positionTMP->x - point2->positionTMP->x;
	float b = point1->positionTMP->y - point2->positionTMP->y;
	float c = point1->positionTMP->z - point2->positionTMP->z;	
	float currentLen = sqrt(a*a + b*b + c*c);
	float newton = -stiffness * (currentLen - initialLength);

	

	force->x = newton * (point1->positionTMP->x - point2->positionTMP->x) / currentLen;
	force->y = newton * (point1->positionTMP->y - point2->positionTMP->y) / currentLen;
	force->z = newton * (point1->positionTMP->z - point2->positionTMP->z) / currentLen;
	addToEndPoints(force);


	//Damping
	point1->force->x -= point1->damping * point1->velocity->x;
	point1->force->y -= point1->damping * point1->velocity->y;
	point1->force->z -= point1->damping * point1->velocity->z;

	point2->force->x -= point2->damping * point2->velocity->x;
	point2->force->y -= point2->damping * point2->velocity->y;
	point2->force->z -= point2->damping * point2->velocity->z;

	free(force);
}
void Spring::addToEndPoints(DirectX::XMFLOAT3 *force){
	point1->force->x += force->x;
	point1->force->y += force->y;
	point1->force->z += force->z;

	point2->force->x -= force->x;
	point2->force->y -= force->y;
	point2->force->z -= force->z;
}

void Spring::reset(){
	point1->reset();
	point2->reset();
}

void Spring::setStiffness(float s){
	stiffness = s;
}