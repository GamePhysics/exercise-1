#pragma once

#include <DirectXMath.h>

class Point
{
public:

	DirectX::XMFLOAT3 *position;
	DirectX::XMFLOAT3 *originalPosition;
	DirectX::XMFLOAT3 *velocity;
	DirectX::XMFLOAT3 *force;
	DirectX::XMFLOAT3 *velocityTMP;
	DirectX::XMFLOAT3 *positionTMP;
	float mass;
	float damping;
	Point(void);
	~Point(void);
	Point(float a, float b, float c);

	void translate(float a, float b, float c);
	void clearForce();
	void addGravity(float gr);
	void reset();
	void setMass(float f);
};

