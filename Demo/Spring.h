#pragma once

#include "Point.h"

class Spring
{
public:
	Spring(void);
	Spring::Spring(Point *p1, Point *p2);
	~Spring(void);
	Point *point1;
	Point *point2;
	float stiffness;
	float initialLength;

	float currentLength();
	void computeElasticForces();
	void computeElasticForcesMidPoint();
	void addToEndPoints(DirectX::XMFLOAT3 *force);
	void reset();
	void setStiffness(float s);
};

