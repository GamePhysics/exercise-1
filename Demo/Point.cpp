#include "Point.h"


Point::Point(void)
{
}

Point::Point(float a, float b, float c)
{   originalPosition = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	position = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));	
	velocity = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	positionTMP = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));	
	velocityTMP = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	force = (DirectX::XMFLOAT3*) malloc(sizeof(DirectX::XMFLOAT3));
	position->x = a;
	position->y = b;
	position->z = c;
	originalPosition->x = a;
	originalPosition->y = b;
	originalPosition->z = c;
	velocity->x = 0;
	velocity->y = 0;
	velocity->z = 0;
	force->x = 0;
	force->y = 0;
	force->z = 0;

	mass = 10;
	damping = 0.5f;
}


Point::~Point(void)
{
	/*if(position != nullptr){
		free(position);
	}
	if(originalPosition != nullptr){
		free(originalPosition);
	}
	if(velocity != nullptr){
		free(velocity);
	}
	if(force != nullptr){
		free(force);
	}*/
}


void Point::translate(float a, float b, float c){
	position->x += a;
	position->y += b;
	position->z += c;
}

void Point::clearForce(){
	force->x = 0;
	force->y = 0;
	force->z = 0;
}

void Point::addGravity(float gr){
	force->y -= gr * mass;
}

void Point::reset(){
	position->x = originalPosition->x;
	position->y = originalPosition->y;
	position->z = originalPosition->z;
	force->x = 0;
	force->y = 0;
	force->z = 0;
	velocity->x = 0;
	velocity->y = 0;
	velocity->z = 0;
}

void Point::setMass(float f){
	mass = f;
}